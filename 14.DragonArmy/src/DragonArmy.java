import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class DragonArmy {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = Integer.valueOf(scanner.nextLine());

        Map<String, TreeMap<String, Dragon>> dragons = new LinkedHashMap<>();

        for (int i = 0; i < n; i++) {

            String[] input = scanner.nextLine().split(" ");
            String type = input[0];
            String name = input[1];
            String damage = input[2];
            String health = input[3];
            String armor = input[4];


            if (!dragons.containsKey(type)) {
                Dragon dragon = new Dragon(name, damage, health, armor);
                TreeMap<String, Dragon> dragonList = new TreeMap<>();
                dragonList.put(dragon.name, dragon);
                dragons.put(type, dragonList);
            } else {
                Dragon dragon = new Dragon(name, damage, health, armor);
                TreeMap<String, Dragon> dragonList = dragons.get(type);
                dragonList.put(dragon.name, dragon);
            }
        }

        for (Map.Entry<String, TreeMap<String, Dragon>> type : dragons.entrySet()) {
            double damageSum = 0d;
            double healtSum = 0d;
            double armorSum = 0d;
            for (Map.Entry<String, Dragon> dragon : type.getValue().entrySet()) {
                damageSum += dragon.getValue().damage;
                healtSum += dragon.getValue().health;
                armorSum += dragon.getValue().armor;
            }
            double damageAvg = damageSum / type.getValue().size();
            double healthAvg = healtSum / type.getValue().size();
            double armorAvg = armorSum / type.getValue().size();
            System.out.printf("%s::(%.2f/%.2f/%.2f)%n", type.getKey(), damageAvg, healthAvg, armorAvg);

            for (Map.Entry<String, Dragon> dragon : type.getValue().entrySet()) {
                System.out.printf("-%s -> damage: %d, health: %d, armor: %d%n", dragon.getValue().name, dragon.getValue().damage, dragon.getValue().health, dragon.getValue().armor);
            }
        }
    }

    private static class Dragon {
        public String name;
        public int health = 250;
        public int damage = 45;
        public int armor = 10;

        public Dragon() {
        }

        public Dragon(String name, String damage, String health, String armor) {
            this.name = name;
            if (!"null".equals(damage))
                this.damage = Integer.valueOf(damage);
            if (!"null".equals(health))
                this.health = Integer.valueOf(health);
            if (!"null".equals(armor))
                this.armor = Integer.valueOf(armor);
        }
    }
}
